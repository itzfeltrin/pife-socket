import socket
from threading import Thread
from utils.constants import HOST


class Client:
    def __init__(self, port):
        self.sock = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM
        )
        self.server_address = (HOST, port)

    def connect(self):
        self.sock.connect(self.server_address)

    def receive_message(self):
        raw_data = self.sock.recv(1024).decode("utf-8")
        messages = raw_data.split("<br>")

        for message_w_code in messages:
            message = message_w_code[6:]

            if '001' in message_w_code:
                response = input(message)
                self.send_message(response)
            elif '002' in message_w_code:
                print(message)

        return raw_data

    def send_message(self, message):
        self.sock.send(message.encode("utf-8"))

    def listen(self):
        try:
            while True:
                self.receive_message()
        except Exception as err:
            print(str(err))
            self.sock.close()

    def start(self):
        confirm_message = self.receive_message()
        if '400' in confirm_message:
            return -1
        else:
            Thread(target=self.listen).start()
