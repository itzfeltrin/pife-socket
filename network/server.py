import socket
from threading import Thread
from model.player import Player
from network.sock import Sock
from utils.constants import NUM_OF_PLAYERS, HOST


class Server:
    def __init__(self, port):
        self.sock = None
        self.server_address = (HOST, port)
        self.players = []
        self.has_running_instance = False

    def start(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.bind(self.server_address)
            print("Iniciando servidor de pife no endereco %s porta %s" % self.server_address)
            self.has_running_instance = False
            self.sock.listen(NUM_OF_PLAYERS)
            nome = input("Você é o primeiro jogador. Insira seu nome: \n>> ")
            player = Player(None, self.server_address, nome)
            self.players.append(player)
        except OSError:
            self.has_running_instance = True

    def run_loop(self):
        while True:
            try:
                player_sock, addr = self.sock.accept()
                player = Player(player_sock, addr, "Desconhecido")
                Thread(target=self.handle_player, args=(player,)).start()
            except KeyboardInterrupt:
                self.sock.close()
                exit(1)

    def handle_player(self, player):
        if len(self.players) < NUM_OF_PLAYERS:
            self.broadcast_message("Novo jogador conectado (%s:%s)" % player.addr)
            Sock.send_message(player.sock, "Bem-vindo ao servidor! Insira seu nome: \n>> ", "001")

            player_name = player.sock.recv(1024).decode("utf-8")
            player.name = player_name

            self.players.append(player)
        else:
            Sock.send_message(player.sock, "Sala cheia", "400")

    def broadcast_message(self, message):
        print(message)
        for player in self.players:
            if player.sock is not None:
                try:
                    Sock.send_message(player.sock, message)
                except Exception as err:
                    print(str(err))

    def change_port(self):
        host, port = self.server_address
        port += 4
        self.server_address = (host, port)
