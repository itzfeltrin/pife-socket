from enum import Enum


NUM_OF_PLAYERS = 2
HOST = 'localhost'
PORT = 3320


class Colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class GameState(Enum):
    NOT_STARTED = 0,
    IN_PROGRESS = 1,
    FINISHED = 2,
