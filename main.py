import gc
from _socket import SHUT_RDWR
from threading import Thread
from network.server import Server
from network.client import Client
from model.game import Game
from utils.constants import NUM_OF_PLAYERS, PORT


def main():
    port = PORT
    while True:
        server = Server(port)
        server.start()

        if server.has_running_instance:
            client = Client(port)
            client.connect()
            conn_result = client.start()

            if conn_result == -1:
                client.sock.close()
                del client
                port += 4
            else:
                del server
                break

            gc.collect()
        else:
            Thread(target=server.run_loop).start()

            last_len = None

            while True:
                if len(server.players) == NUM_OF_PLAYERS:
                    server.broadcast_message("Jogo prestes a começar")
                    break
                elif len(server.players) != last_len:
                    last_len = len(server.players)
                    print(f"Esperando por outros jogadores ({len(server.players)}/{NUM_OF_PLAYERS})")

            game = Game(server)
            game.deck.shuffle()
            game.hand_out_cards()

            # loop para quando um jogo terminar ja comecar outro a menos que
            while game.should_restart:
                game.start()

                # reseta maco, morto e outras informacoes quando o jogo termina
                game.reset()

        server.sock.shutdown(SHUT_RDWR)
        server.sock.close()
        exit(1)


main()
