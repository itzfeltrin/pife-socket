from utils.constants import Colors


card_symbols = []


symbols = [
    ['🂡', '🂢', '🂣', '🂤', '🂥', '🂦', '🂧', '🂨', '🂩', '🂪', '🂫', '🂭', '🂮'],
    ['🂱', '🂲', '🂳', '🂴', '🂵', '🂶', '🂷', '🂸', '🂹', '🂺', '🂻', '🂽', '🂾'],
    ['🃁', '🃂', '🃃', '🃄', '🃅', '🃆', '🃇', '🃈', '🃉', '🃊', '🃋', '🃍', '🃎'],
    ['🃑', '🃒', '🃓', '🃔', '🃕', '🃖', '🃗', '🃘', '🃙', '🃚', '🃛', '🃝', '🃞']
]


class Card:
    def __init__(self, suit, value):
        self.suit = suit
        self.value = value

    def to_string(self):
        color = Colors.FAIL if self.suit == 1 or self.suit == 2 else Colors.ENDC
        return f"{color}{symbols[self.suit][self.value]}{Colors.ENDC}"
