from model.deck import Deck
from utils.constants import GameState


class Game:
    def __init__(self, server):
        self.server = server
        self.should_restart = True
        self.players = server.players
        self.deck = Deck()
        self.dead = []
        self.state = GameState.NOT_STARTED
        self.winner = None

    def reset(self):
        self.deck.reset()
        self.state = GameState.NOT_STARTED
        self.winner = None
        for player in self.players:
            player.reset()
        self.hand_out_cards()

    def hand_out_cards(self):
        for j in range(9):
            for player in self.players:
                player.pick_card(self.deck.cards.pop(0))

    def start(self):
        while self.state != GameState.FINISHED:
            round_res = self.play_round()
            if round_res == GameState.FINISHED:
                break

        self.server.broadcast_message("** GAME OVER **")

        if self.winner is not None:
            self.server.broadcast_message(f"O vencedor é {self.winner.name}! Parabéns")
            self.server.broadcast_message(f"{self.winner.cards_down_to_string()}")

        for player in self.players:
            resp = player.ask_message("Deseja jogar uma nova partida? (s/n) \n>> ")
            if resp.lower() != "s":
                self.should_restart = False
                break

    def play_round(self):
        for player in self.players:
            if self.state == GameState.FINISHED:
                return GameState.FINISHED
            self.server.broadcast_message(f"{player.name} é o jogador da vez")
            player.play(self)

    def grab_from_deck(self):
        last_card = self.deck.cards.pop()
        return last_card

    def grab_from_dead(self):
        last_card = self.dead.pop()
        return last_card

    def print(self):
        for player in self.players:
            print(player.to_string())
