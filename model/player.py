import textwrap

from network.sock import Sock
from utils.constants import GameState


def print_card(card):
    return card.to_string()


def flatten(arr):
    result = []
    for sub_arr in arr:
        for item in sub_arr:
            result.append(item)
    return result


def validate_cards(cards):
    cards = sorted(cards, key=lambda x: x.value)
    result = False

    # check if all cards have the same value but different suit
    for index in range(1, len(cards)):
        curr, last = cards[index], cards[index - 1]
        if curr.value == last.value and curr.suit != last.suit:
            result = True
        else:
            result = False
            break

    if result is True:
        return True

    # check if they are sequential and have the same suit
    for index in range(1, len(cards)):
        curr, last = cards[index], cards[index - 1]
        if curr.value - last.value == 1 and curr.suit == last.suit:
            result = True
        else:
            result = False
            break

    return result


class Player:
    def __init__(self, sock, addr, name):
        self.sock = sock
        self.addr = addr
        self.name = name
        self.cards = []
        self.down = []

    def reset(self):
        self.cards = []
        self.down = []

    def pick_card(self, card):
        self.cards.append(card)

    def play(self, game):
        while True:
            if len(game.dead) > 0:
                self.print_message(f"Morto: {game.dead[-1].to_string()}")
            self.print_message(f"{self.cards_to_string()}")
            if len(self.down) > 0:
                self.print_message(f"{self.cards_down_to_string()}")

            answer = int(
                self.ask_message(textwrap.dedent(f"""
                    Selecione a opção desejada
                    1. Virar carta do maço
                    2. Pegar carta do morto
                    3. Baixar trinca
                    4. Passar vez
                    >> """))
            )

            if answer < 1 or answer > 4:
                self.print_message("Opção invalida")
                pass

            if answer == 1:
                last_deck_card = game.grab_from_deck()
                answer = self.ask_message(f"Carta virada: {last_deck_card.to_string()}\n"
                                          "Deseja pegar a carta virada? (s/n) \n>> ")
                if answer.lower() == "s":
                    index = int(self.ask_message(f"Digite o índice da carta que deseja trocar: (1-{len(self.cards)}) \n>> "))
                    if index < 1 or index > len(self.cards):
                        game.dead.append(last_deck_card)
                        pass
                    else:
                        card_to_swap = self.cards[index - 1]
                        self.cards[index - 1] = last_deck_card
                        game.dead.append(card_to_swap)
                        break
                else:
                    game.dead.append(last_deck_card)
                    break
            elif answer == 2:
                last_dead_card = game.grab_from_dead()
                index = int(self.ask_message(f"Digite o índice da carta que deseja trocar: (1-{len(self.cards)}) \n>> "))
                if index < 1 or index > len(self.cards):
                    game.dead.append(last_dead_card)
                    pass
                else:
                    card_to_swap = self.cards[index - 1]
                    self.cards[index - 1] = last_dead_card
                    game.dead.append(card_to_swap)
                    break
            elif answer == 3:
                card_indexes = []
                while len(card_indexes) != 3:
                    card_indexes = self.ask_message(f"Digite o índice das 3 cartas que deseja baixar: (1-{len(self.cards)}) \n>> ").split()
                card_indexes = map(lambda x: int(x), card_indexes)
                cards = list(map(lambda x: self.cards[x - 1], card_indexes))
                if validate_cards(cards):
                    for card in cards:
                        self.down.append(card)
                        self.cards.remove(card)
                    if len(self.cards) == 0:
                        game.state = GameState.FINISHED
                        game.winner = self
                        break
                    else:
                        pass
                else:
                    self.print_message("Trinca inválida")
                    pass
            elif answer == 4:
                break

    def print_message(self, message, code="002"):
        if self.sock is None:
            print("".join(["\n", message]))
        else:
            Sock.send_message(self.sock, message, code)

    def ask_message(self, message, code="001"):
        if self.sock is None:
            return input(message)
        else:
            Sock.send_message(self.sock, message, code)
            resp = self.sock.recv(1024).decode("utf-8")
            return resp

    def cards_to_string(self):
        return f"Cartas na mão: {' '.join(map(print_card, self.cards))}"

    def cards_down_to_string(self):
        return f"Cartas baixadas: {' '.join(map(print_card, self.down))}"

    def to_string(self):
        return f"Jogador: {self.name} {self.addr}"
