from random import shuffle
from model.card import Card


class Deck:
    def __init__(self):
        self.cards = []
        self.reset()

    def reset(self):
        for i in range(4):
            for j in range(13):
                temp = Card(i, j)
                self.cards.append(temp)
                self.cards.append(temp)

    def shuffle(self):
        shuffle(self.cards)
